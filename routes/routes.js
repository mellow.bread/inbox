'use strict';
module.exports = function(app) {
  var inboxController = require('../controllers/InboxController');

  // inboxController Routes
  app.route('/inbox')
    .get(inboxController.list_all_inbox)

   app.route('/inbox/:match_id')
    .get(inboxController.list_match_inbox)

   app.route('/inbox')
    .post(inboxController.post_an_inbox)

   app.route('/inbox/:match_id')
    .post(inboxController.post_a_message)

};