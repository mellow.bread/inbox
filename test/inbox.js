process.env.NODE_ENV = 'test'
const dotenv = require('dotenv');
const fs = require('fs');
const envConfig = dotenv.parse(fs.readFileSync('.env.test'))
// Warning will override env overlapping keys
for (const k in envConfig) {
  process.env[k] = envConfig[k]
}

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const sqlFixtures = require('sql-fixtures');
const { expect } = require('chai');
const user = require('../models/User');
const like = require('../models/Like');


var dbConfig = {
        client: 'mysql',
        connection: {
        host     : process.env.DB_HOST,
        user     : process.env.DB_USER,
        port     : process.env.DB_PORT,
        password : process.env.DB_PASS,
        database : process.env.DB_NAME
    }
}
var knex = require('knex')(dbConfig);
var users = {
  User: [{
    firstName: "Anthony",
    lastName: "Hopkins",
    email: "anthony.hopkins@club-internet.fr",
    created_date: "2018-09-24"
  },
  {
    firstName: "Anthony2",
    lastName: "Hopkins",
    email: "anthony.hopkin@club-internet.fr",
    created_date: "2018-09-24"
  }],
};
var match = {
  Match: {
    first_user_id: 1,
    second_user_id: 2,
    match_date: "2018-09-24"
  }
}
const should = chai.should();
chai.use(chaiHttp)
describe('Likes', () => {
  before(async () => {
    await knex('Like').del()
    await knex('Caracteristic').del()
    await knex('Message').del()
    await knex.raw('DELETE FROM `Match`')
    await knex.raw('ALTER TABLE ' + '`Match`' + ' AUTO_INCREMENT = 1')
    await knex('User').del()
    await knex.raw('ALTER TABLE ' + 'User' + ' AUTO_INCREMENT = 1')
    await sqlFixtures.create(dbConfig, users);
    await knex.raw("insert into `Match` (`first_user_id`, `match_date`, `second_user_id`) values (1, '2018-09-24', 2)");
  });
    it('should post message', (done) => {
        chai.request(app)
        .post('/inbox')
        .send({
          "from": 2,
          "to": 1,
          "match_id": 1,
          "sent_date": {},
          "created_date": {},
          "content": "Hello !"
        })
        .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
      it('it should get a specific user inbox', (done) => {
        chai.request(app)
        .get('/inbox/1')
        .end((err, res) => {
          console.log(res.body)
          if (res.body) {
            res.should.have.status(200);
            expect(res.body.length).to.equal(1)
            done();
          }
          });
      });
      it('should post a message in an inbox', (done) => {
        chai.request(app)
        .post('/inbox/1')
        .send({
          "from": 2,
          "to": 1,
          "match_id": 1,
          "sent_date": {},
          "created_date": {},
          "content": "Hello !"
        })
        .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
      it('should get all inbox', (done) => {
        chai.request(app)
        .get('/inbox')
        .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
  });